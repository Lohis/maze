from settings import *


def read_and_parse_maze_from_file(maze_filename: str) -> (list, bool):
    """
    Reads a maze from a file into a 2D list of characters

    :param maze_filename: Filename of the maze to read and parse
    :return: A tuple with the loaded maze as a 2D list, and a boolean value for whether the operation was successful
    """
    try:
        with open(maze_filename, "r") as maze_file:
            file_lines = maze_file.readlines()
            maze_as_2d_list = []
            for file_line in file_lines:
                line_characters = []
                for character in file_line:
                    if character == LINE_BREAK:
                        continue
                    line_characters.append(character)
                maze_as_2d_list.append(line_characters)

            return maze_as_2d_list, True
    except Exception as e:
        print("Maze file read exception:", str(e))
        return None, False


def print_maze(maze: list, path: list):
    """
    Prints a maze to console

    :param maze: Maze as a 2D list
    :param path: List of coordinate tuples forming the travelled path
    """
    for row in range(len(maze)):
        line = ""
        for col in range(len(maze[0])):
            if maze[row][col] == MAZE_START:
                line += MAZE_START
            elif maze[row][col] == MAZE_END:
                line += MAZE_END
            elif path and (row, col) in path:
                line += MAZE_BREADCRUMB
            else:
                line += maze[row][col]
        print(line)
