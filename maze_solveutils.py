from settings import *


def find_maze_start(maze: list) -> (int, int):
    """
    Finds the coordinates of the start of a maze

    :rtype: tuple
    :param maze: maze as a 2D list
    :return coordinates: start coordinates as an (int, int) tuple
    """
    for row in range(len(maze)):
        if MAZE_START in maze[row]:
            start = (row, maze[row].index(MAZE_START))
            return start
