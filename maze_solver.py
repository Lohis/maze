import argparse

from settings import *
import maze_io
import maze_validation
import maze_algorithm


def validate_maze(maze: list) -> bool:
    """
    Validates a maze using a series of specific validation functions

    :param maze: A maze as a 2D list
    :return: Boolean value indicating whether the maze passed validation or not
    """
    print("Maze validation...")

    # Starting with an assumption that the maze is valid
    is_maze_valid = True

    if is_maze_valid:
        is_maze_valid = maze_validation.check_maze_characters(maze)

    if is_maze_valid:
        is_maze_valid = maze_validation.check_maze_proportions(maze)

    if is_maze_valid:
        is_maze_valid = maze_validation.check_maze_solution_with_bfs(maze, True)

    return is_maze_valid


def solve_maze(maze_filename: str):
    """
    The top-level program flow: read maze from file -> validate the maze -> try solving the maze with defined max tries.

    :param maze_filename: Filename of the maze to load
    """
    maze, read_successful = maze_io.read_and_parse_maze_from_file(maze_filename)
    if not read_successful:
        print("Could not read the maze file - exiting program")
        exit(1)

    is_maze_valid = validate_maze(maze)

    if not is_maze_valid:
        print("Maze is not valid - exiting program")
        exit(2)

    for person_max_moves in MAX_MOVES:
        print(f"\n=====================================\n\nGuiding {PERSON_NAME} with max. {person_max_moves} moves...")
        is_person_out_of_the_maze = maze_algorithm.solve_maze_by_moving_along_wall(maze, person_max_moves)
        if is_person_out_of_the_maze:
            print(f"{PERSON_NAME} got out of the maze!")
            break
        else:
            print(f"{PERSON_NAME} is still stuck in the maze!")


# PROGRAM ENTRY POINT
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="A program for solving a maze")
    parser.add_argument("maze_filename", type=str, help="The filename of the maze to be solved")
    args = parser.parse_args()
    print("Maze file:", args.maze_filename)
    solve_maze(args.maze_filename)
