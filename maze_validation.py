from collections import deque

from settings import *
import maze_io
import maze_solveutils


def check_maze_characters(maze: list) -> bool:
    """
    Checks that the maze contains only valid characters, one starting point and at least one ending point

    :param maze: Maze as a 2D list
    :return: Boolean value indicating whether the maze passed the check or not
    """
    allowed_characters = [MAZE_WALL, MAZE_PASSAGE, MAZE_START, MAZE_END]
    start_amount = 0
    end_amount = 0

    for row in maze:
        for character in row:
            if character not in allowed_characters:
                print(f"Maze is not valid - unexpected character found: {character}")
                return False
            if character == MAZE_START:
                start_amount += 1
                if start_amount > 1:
                    print("Maze is not valid - it contains more than one starting point")
                    return False
            if character == MAZE_END:
                end_amount += 1

    if start_amount < 1:
        print("Maze is not valid - no starting point found")
        return False
    if end_amount < 1:
        print("Maze is not valid - not ending point(s) found")
        return False

    return True


def check_maze_proportions(maze: list) -> bool:
    """
    Checks that the maze is not empty and that it is rectangular

    :param maze: Maze as a 2D list
    :return: Boolean value indicating whether the maze passed the check or not
    """
    maze_size = len(maze)
    if maze_size < 1:
        print("Maze is not valid - it is empty")
        return False
    expected_row_length = len(maze[0])
    if expected_row_length < 1:
        print("Maze is not valid - it is empty")
        return False
    for row in maze:
        if expected_row_length != len(row):
            print("Maze is not valid - its width is not constant")
            return False

    return True


def check_maze_solution_with_bfs(maze: list, print_solution: bool) -> bool:
    """
    Solves the maze using Breadth-First Search. Finds the optimal solution for the maze if there is a solution at all.

    :param maze: Maze as a 2D list
    :param print_solution: Boolean value indicating whether to print the solution path or not
    :return: Boolean value indicating whether there exists a solution to the maze or not
    """
    end_row, end_col = -1, -1
    solution_move_amount = -1

    # Define the directions in which to move: [up, down, left, right]
    directions = [(-1, 0), (1, 0), (0, -1), (0, 1)]

    # Get the maze size
    rows, cols = len(maze), len(maze[0])

    # Create a new queue for storing the cells that need to be visited
    queue = deque()

    # Find start position and add it into the queue as the "0th" move
    start_row, start_col = maze_solveutils.find_maze_start(maze)
    queue.append((start_row, start_col, 0))

    # Create a visited set to keep track of the cells already visited
    visited = set()
    visited.add((start_row, start_col))

    # Create a dictionary for storing the parent cell for each visited cell
    parent = {}

    # Loop until the end is found, or there isn't left any cells to visit
    while queue:
        row, col, moves = queue.popleft()

        # Check if the maze end is reached
        if maze[row][col] == MAZE_END:
            end_row, end_col = row, col
            solution_move_amount = moves
            break

        # Explore all adjacent cells
        for dr, dc in directions:
            new_row, new_col = row + dr, col + dc

            # Check if the cell can be entered and it's not visited before
            if (0 <= new_row < rows) and (0 <= new_col < cols) and \
                maze[new_row][new_col] != MAZE_WALL and \
                    (new_row, new_col) not in visited:

                # Mark the cell visited and add it to the queue
                visited.add((new_row, new_col))
                queue.append((new_row, new_col, moves + 1))

                # Store the parent cell for this cell
                parent[(new_row, new_col)] = (row, col)

    # If there is a solution, backtrack it to a list of coordinates
    if solution_move_amount > 0:
        solution_path = []
        row, col = end_row, end_col
        solution_path.append((row, col))
        while (row, col) != (start_row, start_col):
            row, col = parent[(row, col)]
            solution_path.append((row, col))

        # Print the optimal solution
        print(f"Optimal solution exists and has {solution_move_amount} moves.")
        if print_solution:
            maze_io.print_maze(maze, solution_path)
        return True

    print("There is no solution to the maze!")
    return False
