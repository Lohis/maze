from settings import *
import maze_solveutils
import maze_io


def solve_maze_by_moving_along_wall(maze: list, max_moves: int) -> bool:
    """
    A revised maze solver algorithm.
    It's a Left-Hand-Rule algorithm, which turns left whenever possible.

    However, it is tuned with some heuristics for determining at every step which direction to choose next:
    - Direction prioritization: left > forward > right > back
    - Tiles not travelled previously get a bonus
    - A direction with a neighboring ending tile gets a massive priority bonus (that's obvious!)
    - A direction leading outside the maze gets a massive priority penalty

    :param maze: Maze as a 2D list
    :param max_moves: Maximum allowed moves
    :return: Boolean value indicating whether the maze was solved or not
    """
    # Define directions: up, right, down, left
    dx = [-1, 0, 1, 0]
    dy = [0, 1, 0, -1]

    # Set the initial orientation up
    orientation = 0

    # Orientation offsets for looking at neighboring tiles: left, forward, right, back
    orientation_offsets = [-1, 0, 1, 2]

    # Score for prioritizing neighboring tiles: left, forward, right, back
    score_choices = [TILE_SCORE_LEFT_TURN, TILE_SCORE_FORWARD, TILE_SCORE_RIGHT_TURN, TILE_SCORE_TURN_BACK]

    x, y = maze_solveutils.find_maze_start(maze)
    path = []
    is_solved = False
    moves = 0

    while moves < max_moves:
        # Priority scores for next move options
        next_move_scores = [0, 0, 0, 0]

        # use orientation offsets list to look left, forward, right and back, and score each option
        for i in range(len(orientation_offsets)):
            tentative_orientation = (orientation + orientation_offsets[i]) % 4
            tentative_x = x + dx[tentative_orientation]
            tentative_y = y + dy[tentative_orientation]

            # Check if this direction leads outside the maze, penalize significantly if it does and don't check further
            if not 0 <= tentative_x < len(maze) or not 0 <= tentative_y < len(maze[0]):
                next_move_scores[tentative_orientation] = TILE_SCORE_PENALTY_OUTSIDE_MAZE
                continue

            direction_score = 0

            # Give a massive priority bonus for a neighboring direction with ending in it, as we really want to go there
            if maze[tentative_x][tentative_y] == MAZE_END:
                direction_score += TILE_SCORE_BONUS_END_TILE

            # Give a big priority bonus to a previously untraveled direction
            if (tentative_x, tentative_y) not in path and maze[tentative_x][tentative_y] == MAZE_PASSAGE:
                direction_score += TILE_SCORE_BONUS_NOT_VISITED

            # Give a direction-based bonus on any valid direction; priority order: left > forward > right > back
            if maze[tentative_x][tentative_y] == MAZE_PASSAGE:
                direction_score += score_choices[i]

            next_move_scores[tentative_orientation] = direction_score

        # Choose the direction with the highest score and step into that neighboring tile
        orientation = next_move_scores.index(max(next_move_scores))
        x = x + dx[orientation]
        y = y + dy[orientation]
        path.append((x, y))
        moves += 1

        if maze[x][y] == MAZE_END:
            is_solved = True
            break

    maze_io.print_maze(maze, path)
    return is_solved
