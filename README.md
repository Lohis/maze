# Maze

- A programming task where Pentti has gotten stuck in a maze
- Implemented with Python 3 - no external libraries required
- First solution of the Pentti-guiding algorithm was submitted on 9.6.2024. Having been able to work only 2 weekend days on it during the one-week time window, I felt dissatisfied with the actual algorithm: while it worked, it was messy and did not perform very well
- The algorithm was revised and greatly improved on 19.6.2024 by working one additional day on it (3 days in total). I direly needed the 10-day period in between to recover from having worked half a year non-stop on Buutti's Full Stack & Cloud program.

## Usage

- You need Python 3 installed on your system (the latest version 3.12 is recommended, but this should work fine with a lot older version as well)
- The solution uses command line

### Running the program

- maze_solver.py is the starting point of the program
- Run the program with Python, and also pass a filename of a maze as an argument
- The maze file is expected to be in the same folder as the program
- Example:
```
python maze_solver.py maze.txt
```

### Providing a maze file

- The maze file is passed as an argument when running the program (see above)
- Mazes are represented in text files (.txt) - one maze per file
- The maze file is expected to have a consistent line length
- The provided maze needs to be rectangular, but does not have to be square
- Only the following characters are allowed in a maze file, representing a wall, passage, start and end:
```
'#', ' ', '^', 'E'
```
- The maze may have 1...n ends, and exactly 1 starting point 

## Execution flow of the program

- Read and parse the maze file
- Validate the maze file: check for unknown characters, valid proportions and solveability
- For checking if there exists a solution for the maze, it is solved using Breadth-First Search; this also finds the optimal solution and prints its route and move amount
- Try solving the maze by simulating Pentti traversing through the maze one move at a time, with maximum move amounts of 20, 150 and 200 moves.

## Current status of the programming task

- Roughly one week of time was given to implement a solution; I was able to get to writing the implementation on the weekend during the last 2 days
- I was pleased with the overall structure and completeness of my solution, even though the actual algorithm for guiding Pentti was not very good at that stage
- One additional day was used later to rewrite the Pentti-guiding algorithm - now I am satisfied with it

## My reasoning behind some of the choices regarding the task

- Instead of focusing just on the algorithm for guiding Pentti, I wanted to emphasize the completeness, cleanliness and documented state of the solution - by doing so, improving or completely replacing the pentti-algorithm is pretty straightforward with more time at hand (which is what I ended up doing)
- While there's no tests provided, I broke the solution down to concise methods that definitely can be tested
- Python is the language I have been using for the last 3 months, so picking it up for the task was the quickest way to start coding
- Python is also easy to run anywhere, especially in this case where the project does not have external dependencies (no need to set up venv's)
- Python also has very expressive basic data structures, which helped a lot when implementing the solution
- However, for larger projects Python needs extra careful coding due to lack of type safety and other "quick to get started" features starting to backfire
- Ideally, I would have liked to try C# with .NET Core and Object-Oriented approach, but stayed on Python to save time

## Tooling

- Arch Linux
- Python 3.12
- Git CLI
- GitLab
- PyCharm Community Edition
- Locally run Codestral-22B Large Language Model for sparring me with Python
